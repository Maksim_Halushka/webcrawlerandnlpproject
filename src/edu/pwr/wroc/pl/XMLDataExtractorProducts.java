package edu.pwr.wroc.pl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDataExtractorProducts extends XMLDataExtractor {
	
	private Collection<ProductItem> productItemList;
	
	public XMLDataExtractorProducts(String fileName) throws ParserConfigurationException, SAXException, IOException {
		super(fileName);
	}
	
	public Collection<ProductItem> getProductList() throws XPathExpressionException {
		productItemList = new ArrayList<ProductItem>();
		ProductItem empty = new ProductItem();
		empty.setName("<<Empty>>");
		productItemList.add(empty);
		// create XPathExpression object
		XPathExpression expr = getXpath().compile("/Allitems/Item/*");
		// evaluate expression result on XML document
		NodeList nodes = (NodeList) expr.evaluate(getDocument(), XPathConstants.NODESET);
		Node pwrNode = null;
		ProductItem pi = new ProductItem();
		for (int i = 0; i < nodes.getLength(); i++) {
			pwrNode = nodes.item(i);
			if(pwrNode != null && pwrNode.getFirstChild() != null) {
				String nodeKey = pwrNode.getFirstChild().getNodeValue();
				switch (pwrNode.getNodeName()) {
					case NODE_NAME_PRODUCT_NAME:
						pi = new ProductItem();
						pi.setName(nodeKey);
						break;
					case NODE_NAME_PRICE:
						pi.setPrice(nodeKey);
						break;
					case NODE_NAME_REVIEW_COUNT:
						pi.setReviewCount(Integer.parseInt(nodeKey));
						break;
					case NODE_NAME_PRODUCT_TYPE:
						pi.setProductType(nodeKey);
						break;
					default:
						break;
				}
				if(!getProductItemList().contains(pi))
					getProductItemList().add(pi);
			}
		}
		return productItemList;
	}

	public Collection<ProductItem> getProductListByProducer(String producerName) {
		Collection<ProductItem> localProductItemList = new ArrayList<ProductItem>();
		ProductItem empty = new ProductItem();
		empty.setName("<<Empty>>");
		localProductItemList.add(empty);
		for (ProductItem productItem : getProductItemList()) {
			if(productItem.getName().contains(producerName))
				localProductItemList.add(productItem);
		}
		return localProductItemList;
	}

	public Collection<ProductItem> getProductItemList() {
		return productItemList;
	}

	public void setProductItemList(Collection<ProductItem> productItemList) {
		this.productItemList = productItemList;
	}
	
}
