package edu.pwr.wroc.pl;

public class ConstantValues {
	public static String SEPARATOR = "|";
	public static String SEPARATOR_ADD = "+";
	
	static {
		for (int i = 0; i < 10; i++)
			SEPARATOR += "-";
		SEPARATOR += "> ";
		for (int i = 0; i < 10; i++)
			SEPARATOR_ADD += "-";
		SEPARATOR_ADD += "> ";
	}
	
	/**
	 * Server configurations, paths and other.
	 * This values show what paths and configurations in system exist.
	 * **/
	public static final String FTP_DATA_XML_NAME = "ProductExtractor-AllItems.xml";
	public static final String LOCAL_PREFIX_PATH = "input/";
	public static final String DISC_PREFIX_PATH = "E:/ftp/";
	public static final String FTP_DATA_PATH = "data/";
	public static final String TXT_SOURCE_FILE = "Review_mark.txt";
	public static final String[] EMPTY_STRING = {""};

	public static final String INFO_EMPTY = "--EMPTY--";
	public static final String INFO_SPACE = " ";
	public static final String INFO_STATS = "Stats:";
	public static final String INFO_V_NEGATIVE = "V. negative:";
	public static final String INFO_NEGATIVE = "Negative:";
	public static final String INFO_NEUTRAL = "Neutral:";
	public static final String INFO_POSITIVE = "Positive:";
	public static final String INFO_V_POSITIVE = "V. positive:";
	public static final String INFO_OPEN_NLP = "OpenNLP > ";
	public static final String INFO_STANFORD_NLP = "Stanford NLP > ";
	
	public static final String REGEXP_MARK_WITH_ARROW = "[0-4] --> ";
	public static final String REGEXP_ARROW = " --> ";
	public static final String SIMPLE_SIGN = "-";
	
	// Variables for case structure in split algorithm
	public static final int ALGORITHM_SPLIT = 10000;
	public static final int ALGORITHM_SPLIT_REMOVE_COMPANY = 10001;
	public static final int ALGORITHM_SPLIT_MEDIAN = 10002;
	
	// How many words can match in opinion
	public static final int MATCH_WORD_COUNT = 2;
	
	// 1 - IS FOR REVIEW TITLE
	public static final int REVIEW_TYPE_TITLE = 1;
	// 2 - IS FOR REVIEW DESCRIPTION
	public static final int REVIEW_TYPE_DESCRIPTION = 2;

	public static final int STAT_DIFF_PARAM_NEG_4 = -4;
	public static final int STAT_DIFF_PARAM_NEG_3 = -3;
	public static final int STAT_DIFF_PARAM_NEG_2 = -2;
	public static final int STAT_DIFF_PARAM_NEG_1 = -1;
	public static final int STAT_DIFF_PARAM_NEU_0 = 0;
	public static final int STAT_DIFF_PARAM_POS_1 = 1;
	public static final int STAT_DIFF_PARAM_POS_2 = 2;
	public static final int STAT_DIFF_PARAM_POS_3 = 3;
	public static final int STAT_DIFF_PARAM_POS_4 = 4;
	
	public final static int[] STAT_TABLE_OF_PARAM = {
		STAT_DIFF_PARAM_NEG_4,
		STAT_DIFF_PARAM_NEG_3,
		STAT_DIFF_PARAM_NEG_2,
		STAT_DIFF_PARAM_NEG_1,
		STAT_DIFF_PARAM_NEU_0,
		STAT_DIFF_PARAM_POS_1,
		STAT_DIFF_PARAM_POS_2,
		STAT_DIFF_PARAM_POS_3,
		STAT_DIFF_PARAM_POS_4
	};
	
	
}
