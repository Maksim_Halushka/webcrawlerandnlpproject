package edu.pwr.wroc.pl;

import java.util.ArrayList;
import java.util.List;

public class ProductWithSentimentItem extends ProductItem {

	private int expertSentiment;
	private int stanfordNLPSentiment;
	private int openNLPSentiment;
	private int splitNLPSentiment;
	private int splitNLPSentimentNoCompany;
	private int splitNLPSentimentNoCompanyMedian;
	private int reviewType;
	private int reviewConnector;
	private String productReview;
	private List<String> reviewOtherProductArray;
	private List<String> reviewOtherProducerArray;

	public ProductWithSentimentItem() {
		super();
		this.setExpertSentiment(-1);
		this.setStanfordNLPSentiment(-1);
		this.setOpenNLPSentiment(-1);
		this.setReviewType(-1);
		this.setReviewConnector(-1);
		this.setSplitNLPSentiment(-1);
		this.setSplitNLPSentimentNoCompany(-1);
		this.setSplitNLPSentimentNoCompanyMedian(-1);
		this.setProductReview(null);
		this.setReviewOtherProductArray(new ArrayList<String>());
		this.setReviewOtherProducerArray(new ArrayList<String>());
	}

	public ProductWithSentimentItem(String name, String price, int reviewCount, String productType, String productProducer, List<String[]> reviewList) {
		super(name, price, reviewCount, productType, productProducer, reviewList);
	}
	
	public ProductWithSentimentItem(int expertSentiment,
			int sentimentStanfordNLP, int openNLPSentiment, int reviewType,
			int reviewConnector, int generalNLPSentiment, String productReview, List<String> reviewOtherProductArray, List<String> reviewOtherProducerArray) {
		super();
		setExpertSentiment(expertSentiment);
		setStanfordNLPSentiment(sentimentStanfordNLP);
		setOpenNLPSentiment(openNLPSentiment);
		setReviewType(reviewType);
		setReviewConnector(reviewConnector);
		setSplitNLPSentiment(generalNLPSentiment);
		setProductReview(productReview);
		setReviewOtherProductArray(reviewOtherProductArray);
		setReviewOtherProducerArray(reviewOtherProducerArray);
	}

	public int getExpertSentiment() {
		return expertSentiment;
	}

	public void setExpertSentiment(int expertSentiment) {
		this.expertSentiment = expertSentiment;
	}

	public int getOpenNLPSentiment() {
		return openNLPSentiment;
	}

	public void setOpenNLPSentiment(int openNLPSentiment) {
		this.openNLPSentiment = openNLPSentiment;
	}

	public int getReviewConnector() {
		return reviewConnector;
	}

	public void setReviewConnector(int reviewConnector) {
		this.reviewConnector = reviewConnector;
	}

	public int getReviewType() {
		return reviewType;
	}

	public void setReviewType(int reviewType) {
		this.reviewType = reviewType;
	}

	public int getSplitNLPSentiment() {
		return splitNLPSentiment;
	}

	public void setSplitNLPSentiment(int splitNLPSentiment) {
		this.splitNLPSentiment = splitNLPSentiment;
	}

	public int getSplitNLPSentimentNoCompany() {
		return splitNLPSentimentNoCompany;
	}

	public void setSplitNLPSentimentNoCompany(int splitNLPSentimentNoCompany) {
		this.splitNLPSentimentNoCompany = splitNLPSentimentNoCompany;
	}

	public int getSplitNLPSentimentNoCompanyMedian() {
		return splitNLPSentimentNoCompanyMedian;
	}

	public void setSplitNLPSentimentNoCompanyMedian(
			int splitNLPSentimentNoCompanyMedian) {
		this.splitNLPSentimentNoCompanyMedian = splitNLPSentimentNoCompanyMedian;
	}

	public int getStanfordNLPSentiment() {
		return stanfordNLPSentiment;
	}

	public void setStanfordNLPSentiment(int stanfordNLPSentiment) {
		this.stanfordNLPSentiment = stanfordNLPSentiment;
	}

	
	public String getProductReview() {
		return productReview;
	}

	
	public void setProductReview(String productReview) {
		this.productReview = productReview;
	}

	
	public List<String> getReviewOtherProductArray() {
		return reviewOtherProductArray;
	}

	
	public void setReviewOtherProductArray(List<String> reviewOtherProductArray) {
		this.reviewOtherProductArray = reviewOtherProductArray;
	}
	
	public void addToReviewOtherProductArray(String reviewOtherProductString) {
		reviewOtherProductArray.add(reviewOtherProductString);
	}
	
	public void removeFromReviewOtherProductArray(String reviewOtherProductString) {
		this.reviewOtherProductArray.remove(reviewOtherProductString);
	}

	public List<String> getReviewOtherProducerArray() {
		return reviewOtherProducerArray;
	}

	public void setReviewOtherProducerArray(List<String> reviewOtherProducerArray) {
		this.reviewOtherProducerArray = reviewOtherProducerArray;
	}
	
}
