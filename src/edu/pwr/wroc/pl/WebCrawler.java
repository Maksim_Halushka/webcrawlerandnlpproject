package edu.pwr.wroc.pl;

import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class WebCrawler {
	
	private static XMLDataExtractor EXTRACTOR_PRODUCER;
	private static XMLDataExtractor EXTRACTOR_PRODUCT;
	private static XMLDataExtractor EXTRACTOR_REVIEW;

	public static void main(String[] args) {
		NLP.init();
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		try {
			EXTRACTOR_PRODUCER = new XMLDataExtractorProducers("ProductAndProducer.xml");
			EXTRACTOR_PRODUCT = new XMLDataExtractorProducts("ProductNamesDiy-AllItems.xml");
			EXTRACTOR_REVIEW = new XMLDataExtractorReviews("ProductNamesDiy-AllItemsReview.xml");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				GUI ex = new GUI(EXTRACTOR_PRODUCER, EXTRACTOR_PRODUCT, EXTRACTOR_REVIEW);
				ex.setVisible(true);
			}
		});
	}

}
