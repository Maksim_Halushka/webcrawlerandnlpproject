package edu.pwr.wroc.pl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDataExtractorProducers extends XMLDataExtractor {

	public XMLDataExtractorProducers(String fileName) throws ParserConfigurationException, SAXException, IOException {
		super(fileName);
	}
	
	public Collection<String> getProductProducers() throws XPathExpressionException {
		Collection<String> producerList = new ArrayList<String>();
		producerList.add("<<Empty>>");
		// create XPathExpression object
		XPathExpression expr = getXpath().compile("/Allitems/Item/*");
		// evaluate expression result on XML document
		NodeList nodes = (NodeList) expr.evaluate(getDocument(), XPathConstants.NODESET);
		Node pwrNode = null;
		for (int i = 0; i < nodes.getLength(); i++) {
			pwrNode = nodes.item(i);
			if(pwrNode != null && pwrNode.getFirstChild() != null) {
				String nodeKey = pwrNode.getFirstChild().getNodeValue();
				if(!producerList.contains(nodeKey))
					producerList.add(nodeKey);
			}
		}
		return producerList;
	}

}
