package edu.pwr.wroc.pl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDataExtractorReviews extends XMLDataExtractor {

	private Collection<ProductWithSentimentItem> productItemList;
	
	public XMLDataExtractorReviews(String fileName) throws ParserConfigurationException, SAXException, IOException {
		super(fileName);
	}
	
	public Collection<ProductWithSentimentItem> getProductReviewByType(String type) throws XPathExpressionException {
		productItemList = new ArrayList<ProductWithSentimentItem>();
		// create XPathExpression object
		XPathExpression expr = getXpath().compile("/Allitems/Item/ProductName[contains(.,'" + type + "')]");
		// evaluate expression result on XML document
		NodeList nodes = (NodeList) expr.evaluate(getDocument(), XPathConstants.NODESET);
		Node pwrNode = null;
		ProductWithSentimentItem pi = new ProductWithSentimentItem();
		String review = "";
		for (int i = 0; i < nodes.getLength(); i++) {
			pwrNode = nodes.item(i).getParentNode();
			NodeList tmpItemList = pwrNode.getChildNodes();
			for (int j = 0; j < tmpItemList.getLength(); j++) {	
				Node pwrTmpNode = tmpItemList.item(j);
				if(pwrTmpNode != null && pwrTmpNode.getFirstChild() != null) {
					String nodeKey = pwrTmpNode.getFirstChild().getNodeValue();
					switch (pwrTmpNode.getNodeName()) {
						case NODE_NAME_PRODUCT_NAME:
							pi = new ProductWithSentimentItem();
							pi.setName(nodeKey);
							break;
						case NODE_NAME_PRICE:
							pi.setPrice(nodeKey);
							break;
						case NODE_NAME_REVIEW_COUNT:
							pi.setReviewCount(Integer.parseInt(nodeKey));
							break;
						case NODE_NAME_PRODUCT_TYPE:
							pi.setProductType(nodeKey);
							break;
						case NODE_NAME_REVIEW_TITLE:
							if(nodeKey != null && !nodeKey.equals(""))
								review += nodeKey + "|";
							break;
						case NODE_NAME_REVIEW_SUMMARY:
							if(nodeKey != null && !nodeKey.equals("")) {
								review += nodeKey;
								pi.setProductReview(review);
							}
							review = "";
							break;
						default:
							break;
					}
				}
			}
			if(!getProductItemList().contains(pi))
				getProductItemList().add(pi);
		}
		return productItemList;
	}

	public Collection<ProductWithSentimentItem> getProductItemList() {
		return productItemList;
	}

	public void setProductItemList(
			Collection<ProductWithSentimentItem> productItemList) {
		this.productItemList = productItemList;
	}

}
