package edu.pwr.wroc.pl;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLDataExtractor {

	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private Document document;
	private XPathFactory xpathFactory;
	private XPath xpath;
	
	public final static String NODE_NAME_PRODUCT_NAME = "ProductName";
	public final static String NODE_NAME_PRICE = "Price";
	public final static String NODE_NAME_REVIEW_COUNT = "ReviewCount";
	public final static String NODE_NAME_PRODUCT_TYPE = "ProductType";
	public final static String NODE_NAME_REVIEW_TITLE = "ReviewTitle";
	public final static String NODE_NAME_REVIEW_SUMMARY = "ReviewSummary";
	
	
	public XMLDataExtractor(String fileName) throws ParserConfigurationException, SAXException, IOException {
		super();
		factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		builder = factory.newDocumentBuilder();
		// TODO: change this method to get ftp path correctly not from disc
		File xmlSource = new File(/*ConstantValues.DISC_PREFIX_PATH + ConstantValues.FTP_DATA_PATH*/ ConstantValues.LOCAL_PREFIX_PATH + fileName);
		if (xmlSource.isFile() && xmlSource != null)
			document = builder.parse(xmlSource);
		else
			throw new IOException("Can not read source file");
		xpathFactory = XPathFactory.newInstance();
		xpath = xpathFactory.newXPath();
	}

	public DocumentBuilderFactory getFactory() {
		return factory;
	}

	public void setFactory(DocumentBuilderFactory factory) {
		this.factory = factory;
	}

	public DocumentBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(DocumentBuilder builder) {
		this.builder = builder;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public XPathFactory getXpathFactory() {
		return xpathFactory;
	}

	public void setXpathFactory(XPathFactory xpathFactory) {
		this.xpathFactory = xpathFactory;
	}

	public XPath getXpath() {
		return xpath;
	}

	public void setXpath(XPath xpath) {
		this.xpath = xpath;
	}
}