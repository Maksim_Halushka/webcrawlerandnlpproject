package edu.pwr.wroc.pl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

public class NLP {
	static StanfordCoreNLP pipeline;

	public static void init() {
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref, sentiment");
		pipeline = new StanfordCoreNLP(props);
	}

	public static int findSentiment(String text) {

		int mainSentiment = 0;
		if (text != null && text.length() > 0) {
			int longest = 0;
			// Wst�pne procesowanie ci�gu znak�w
			Annotation annotation = pipeline.process(text);
			// Ka�de zdanie z tekstu musi by� odpowiednio przygotowane do analizy
			for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
				// Dzewo sentyment�w jest budowane
				Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
				// Klasa predykcji dla drzewa
				int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
				String partText = sentence.toString();
				if (partText.length() > longest) {
					mainSentiment = sentiment;
					longest = partText.length();
				}

			}
		}
		return mainSentiment;
	}
	
	public static List<String> getSentenceListFromString(String text) {
	    
		// Tworzenie pustej adnotacji z podanego tekstu
		Annotation document = new Annotation(text);
		ArrayList<String> sentencList = new ArrayList<String>();
		// Wykonanie wszystkich w�a�ciwo�ci, przetwarzanie tekstu dokumentu
		pipeline.annotate(document);

		// Pobranie listy zda� po adnotacji zda�
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap coreMap : sentences) {
			Annotation an = (Annotation) coreMap;
			sentencList.add(an.toString());
		}
		return sentencList;
	}
}
