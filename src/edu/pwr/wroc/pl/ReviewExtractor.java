package edu.pwr.wroc.pl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.html.HTML.Tag;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ReviewExtractor {
	
	public final static String URL_SEARCH_GENERAL_PARAM ="{#sParam_unique_label#}";
	public final static String STORE_DIY_PAGINATION_CLASS = "paginator";
	public final static String STORE_DIY_PRODUCT_CLASS = "product-description";
	public final static String URL_DIY_STORE = "http://www.diy.com";
	public final static String URL_DIY_SEARCH = "http://www.diy.com/search/results/";
	public final static String URL_DIY_SEARCH_GENERAL = "http://www.diy.com/search/results/?question=" + URL_SEARCH_GENERAL_PARAM;	
	
	
	private Document startDoc;
	private Elements docElementsList;
	private Elements docNavigationList;

	
	public ReviewExtractor() {
		startDoc = null;
		docElementsList = null;
		docNavigationList = null;
	}
	
	public List<String> getProductReviewByName(String productName) {
		List<String> productReviewList = null;
		try {
			String searchURL = URL_DIY_SEARCH_GENERAL.replace(URL_SEARCH_GENERAL_PARAM, productName);
			startDoc = Jsoup.connect(searchURL).get();			
			String pageIndex = null;
			if(startDoc != null) {	
				productReviewList = new ArrayList<String>();	
				boolean isLoopEnd = true;
				while(isLoopEnd) {
					docNavigationList = startDoc.getElementsByClass(STORE_DIY_PAGINATION_CLASS);
					if(docNavigationList.isEmpty() && productName.indexOf(" ") > -1) {
						productName = productName.substring(0, productName.lastIndexOf(" "));
						searchURL = URL_DIY_SEARCH_GENERAL.replace(URL_SEARCH_GENERAL_PARAM, productName);
						startDoc = Jsoup.connect(searchURL).get();
						docNavigationList = null;
						continue;
					}
					isLoopEnd = false;
					pageIndex = findNextPageURLParameterForCustorama(docNavigationList);				
					docElementsList = startDoc.getElementsByClass(STORE_DIY_PRODUCT_CLASS);
				}
				int globalCounter = 0;
				while (pageIndex != null || globalCounter != 1000) {
					for (Element currentProduct : docElementsList) {
						Elements linkListForProduct = currentProduct.getElementsByTag(Tag.A.toString());
						for (Element link : linkListForProduct) {
							String linkFullText = link.text().toLowerCase();
							if(linkFullText.contains(productName.toLowerCase())) {
								String productURL = URL_DIY_STORE + link.attr("href") + "?tab=reviews";
								Document productDescription = Jsoup.connect(productURL).get();
								Elements review = productDescription.getElementsByClass("bv-content-list-Reviews");
								Elements reviewList = review.get(0).getElementsByTag("bv-content-core");
								if(!reviewList.isEmpty() && reviewList != null) {
									for (Element element : reviewList) {
										Elements reviewTitle = element.getElementsByTag("bv-content-title-container");
										Elements reviewSummary = element.getElementsByTag("bv-content-summary");
										if(!reviewSummary.isEmpty() && reviewSummary != null) {
											String reviewTextSummary = "";
											if (!reviewTitle.text().equals("") && reviewTitle.text() != null)
												reviewTextSummary += reviewTitle.text();
											if (!reviewSummary.text().equals("") && reviewSummary.text() != null)
												reviewTextSummary += "||" + reviewSummary.text();
											if(!productReviewList.contains(reviewTextSummary))
												productReviewList.add(reviewTextSummary);
										}
									}
								}
								break;
							} else 
								continue;
						}
					}
					pageIndex = findNextPageURLParameterForCustorama(docNavigationList);
					if(pageIndex != null) {
						String newURL = URL_DIY_SEARCH + pageIndex.replace(URL_SEARCH_GENERAL_PARAM, productName);
						startDoc = Jsoup.connect(newURL).get();
						docNavigationList = startDoc.getElementsByClass(STORE_DIY_PAGINATION_CLASS);			
						docElementsList = startDoc.getElementsByClass(STORE_DIY_PRODUCT_CLASS);
					} else
						break;
					globalCounter++;
				}
			}
		} catch (IOException e) {
			Logger.getLogger(ReviewExtractor.class.getName()).log(Level.SEVERE, null, e);
		}
		if (productReviewList != null && !productReviewList.isEmpty()) {
			for (String element : productReviewList) {
				System.out.println(element);
			}
		}
		return productReviewList;
	}
	
	private String findNextPageURLParameterForCustorama(Elements docNavigationList) {
		String pageIndex = null;
		for (Element navigation : docNavigationList) { 
			Elements pages = navigation.getElementsByClass("next-page");
			for (Element page : pages) {
				pageIndex = page.attr("href");
				break;
			}
			if(pageIndex != null) {
				String tmpURL = pageIndex.substring(pageIndex.indexOf("page="), pageIndex.indexOf("?question"));
				tmpURL += "?question=" + URL_SEARCH_GENERAL_PARAM;
				tmpURL += "&pageSize=12";
				pageIndex = tmpURL;
				break;
			} else
				continue;
		}
		return pageIndex;
	}
}
