package edu.pwr.wroc.pl;

import java.util.ArrayList;
import java.util.List;

public class ProductItem {

	private String name;
	private String price;
	private int reviewCount;
	private String productType;
	private String productProducer;
	private List<String[]> reviewList;
	private List<ProductWithSentimentItem> reviewListBetter;
	private List<ProductWithSentimentItem> reviewListWorst;
	private List<ProductWithSentimentItem> reviewListUndef;
	

	public ProductItem(String name, String price, int reviewCount, String productType, String productProducer, List<String[]> reviewList) {
		super();
		this.name = name;
		this.price = price;
		this.reviewCount = reviewCount;
		this.productType = productType;
		this.productProducer = productProducer;
		this.setReviewList(reviewList);
	}

	public ProductItem() {
		super();
		this.name = null;
		this.price = null;
		this.reviewCount = new Integer(0);
		this.productType = null;
		this.productProducer = null;
		this.setReviewList(new ArrayList<String[]>());
		this.setReviewListUndef(new ArrayList<ProductWithSentimentItem>());
		this.setReviewListBetter(new ArrayList<ProductWithSentimentItem>());
		this.setReviewListWorst(new ArrayList<ProductWithSentimentItem>());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductProducer() {
		return productProducer;
	}

	public void setProductProducer(String productProducer) {
		this.productProducer = productProducer;
	}
	
	public List<String[]> getReviewList() {
		return reviewList;
	}

	public void setReviewList(List<String[]> reviewList) {
		this.reviewList = reviewList;
	}
	
	public void addToReviewList(String[] array) {
		reviewList.add(array);
	}
	
	public void removeFromReviewList(String[] array) {
		reviewList.remove(array);
	}

	
	public List<ProductWithSentimentItem> getReviewListBetter() {
		return reviewListBetter;
	}

	
	public void setReviewListBetter(List<ProductWithSentimentItem> reviewListBetter) {
		this.reviewListBetter = reviewListBetter;
	}
	
	public void addToReviewListBetter(ProductWithSentimentItem productWithSentimentItem) {
		if (!reviewListBetter.contains(productWithSentimentItem))
			this.reviewListBetter.add(productWithSentimentItem);
	}
	
	public void removeFromReviewListBetter(ProductWithSentimentItem productWithSentimentItem) {
		this.reviewListBetter.remove(productWithSentimentItem);
	}

	public List<ProductWithSentimentItem> getReviewListWorst() {
		return reviewListWorst;
	}

	public void setReviewListWorst(List<ProductWithSentimentItem> reviewListWorst) {
		this.reviewListWorst = reviewListWorst;
	}
	
	public void addToReviewListWorst(ProductWithSentimentItem productWithSentimentItem) {
		if (!reviewListWorst.contains(productWithSentimentItem))
			this.reviewListWorst.add(productWithSentimentItem);
	}

	public void removeFromReviewListWorst(ProductWithSentimentItem productWithSentimentItem) {
		this.reviewListWorst.remove(productWithSentimentItem);
	}

	public List<ProductWithSentimentItem> getReviewListUndef() {
		return reviewListUndef;
	}

	public void setReviewListUndef(List<ProductWithSentimentItem> reviewListUndef) {
		this.reviewListUndef = reviewListUndef;
	}
	
	public void addToReviewListUndef(ProductWithSentimentItem productWithSentimentItem) {
		if (!reviewListUndef.contains(productWithSentimentItem))
			this.reviewListUndef.add(productWithSentimentItem);
	}

	public void removeFromReviewListUndef(ProductWithSentimentItem productWithSentimentItem) {
		this.reviewListUndef.remove(productWithSentimentItem);
	}


	public String arrOfProductsToString(List<ProductWithSentimentItem> productWithSentimentItemArr) {
		String returnString = new String();
		for (ProductWithSentimentItem productWithSentimentItem : productWithSentimentItemArr) {
			returnString += "\n\t P. name: \t" + productWithSentimentItem.getName() 
							+ "\n\t P. producer: \t" + productWithSentimentItem.getProductType() 
							+ "\n\t P. full review: \t" + productWithSentimentItem.getProductReview();
					
		}
		return returnString;
	}
}
