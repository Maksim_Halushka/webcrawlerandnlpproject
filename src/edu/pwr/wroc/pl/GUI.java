package edu.pwr.wroc.pl;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JComboBox;
import javax.xml.xpath.XPathExpressionException;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.FlowLayout;

public class GUI extends JFrame {

	private static final long serialVersionUID = 6918587352728548334L;
	private JPanel topInfoPanel;
	private JPanel rightComboSelectPanel;
	private JPanel centerReviewAndInfoPanel;
	private JComboBox<Object> typeComboboxPanel;
	private XMLDataExtractorProducers xmlDEProducers;
	private XMLDataExtractorProducts xmlDEProducts;
	private XMLDataExtractorReviews xmlDEReviews;
	private JComboBox<Object> productFromTypeComboboxPanel;
	private JLabel infoLabel;
	private JPanel bottomInfoPanel;
	private JButton btnGetReviewByType;
	private JButton btnGetReviewByProduct;
	private JTable tableProduct;
	private JScrollPane scrollPane;
	public static final String[] TABLE_NAMES = new String[] {"Product Name", "Price", "Review \u2116", "Producer"};
	private JLabel topStatsStanfordNLPLabel;
	private JLabel topStatsOpenNLPLabel;
	private JButton btnGetReviewStats;
	
	private int estimationSimpleSplit;
	private int estimationSimpleSplitWithoutCompany;
	private int estimationSimpleSplitWithoutCompanyMedian;
	private List<String> reviewOtherProductArray;

	public GUI(XMLDataExtractor xmlDEProducers, XMLDataExtractor xmlDEProducts, XMLDataExtractor xmlDEReviews) {
		BorderLayout borderLayout = (BorderLayout) getContentPane().getLayout();
		borderLayout.setVgap(8);
		borderLayout.setHgap(8);
		this.xmlDEProducers = (XMLDataExtractorProducers) xmlDEProducers;
		this.xmlDEProducts = (XMLDataExtractorProducts) xmlDEProducts;
		this.xmlDEReviews = (XMLDataExtractorReviews) xmlDEReviews;
		setEstimationSimpleSplit(-1);
		setEstimationSimpleSplitWithoutCompany(-1);
		initUI();
	}

	private void initUI() {
		
		setResizable(false);
		setTitle("Web Extractor");
		setSize(512, 352);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		topInfoPanel = new JPanel();
		getContentPane().add(topInfoPanel, BorderLayout.NORTH);
		topInfoPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		infoLabel = new JLabel("Web crawler App, get info from pages");
		infoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		topInfoPanel.add(infoLabel);
		
		rightComboSelectPanel = new JPanel();
		getContentPane().add(rightComboSelectPanel, BorderLayout.EAST);
		
		typeComboboxPanel = new JComboBox<Object>();
		typeComboboxPanel.setPreferredSize(new Dimension(128, 20));
		String problemElemnt = null;
		try {
			for (String element : xmlDEProducers.getProductProducers()) {
				problemElemnt = element;
				typeComboboxPanel.addItem(element);
			}
		} catch (XPathExpressionException e) {
			System.err.println("Problem wiht: " + problemElemnt);
			e.printStackTrace();
		}
		rightComboSelectPanel.setLayout(new GridLayout(8, 1, 4, 8));
		TypeComboItemListener itemTypeListener = new TypeComboItemListener();
		typeComboboxPanel.addItemListener(itemTypeListener);
		rightComboSelectPanel.add(typeComboboxPanel);
		
		productFromTypeComboboxPanel = new JComboBox<Object>();
		productFromTypeComboboxPanel.setEnabled(false);
		productFromTypeComboboxPanel.setPreferredSize(new Dimension(128, 20));
		problemElemnt = null;
		try {
			Collection<ProductItem> productItemList = xmlDEProducts.getProductList();
			for (ProductItem element : productItemList) {
				problemElemnt = element.getName();
				productFromTypeComboboxPanel.addItem(element.getName());
			}
		} catch (XPathExpressionException e) {
			System.err.println("Problem wiht: " + problemElemnt);
			e.printStackTrace();
		}
		ProductComboItemListener itemProductListener = new ProductComboItemListener();
		productFromTypeComboboxPanel.addItemListener(itemProductListener);
		rightComboSelectPanel.add(productFromTypeComboboxPanel);
		
		btnGetReviewByType = new JButton("Get review by type");
		btnGetReviewByType.setEnabled(false);
		StatsByTypeActionListener sbtAL = new StatsByTypeActionListener();
		btnGetReviewByType.addActionListener(sbtAL);
		rightComboSelectPanel.add(btnGetReviewByType);
		
		btnGetReviewByProduct = new JButton("Get review by product");
		btnGetReviewByProduct.setEnabled(false);
		StatsByProductActionListener sbpAL = new StatsByProductActionListener();
		btnGetReviewByProduct.addActionListener(sbpAL);
		rightComboSelectPanel.add(btnGetReviewByProduct);
		
		btnGetReviewStats = new JButton("Get review stats");
		StatsByFileActionListener sbfAL = new StatsByFileActionListener();
		btnGetReviewStats.addActionListener(sbfAL);
		rightComboSelectPanel.add(btnGetReviewStats);
		
		centerReviewAndInfoPanel = new JPanel();
		getContentPane().add(centerReviewAndInfoPanel, BorderLayout.CENTER);
		centerReviewAndInfoPanel.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		scrollPane.setMaximumSize(new Dimension(128, 128));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		centerReviewAndInfoPanel.add(scrollPane);
		
		tableProduct = new JTable();
		scrollPane.setViewportView(tableProduct);
		tableProduct.setRowSelectionAllowed(true);
		ListSelectionModel cellSelectionModel = tableProduct.getSelectionModel();
	    cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {

				int[] selectedRow = tableProduct.getSelectedRows();
				
				if(selectedRow.length > 0) {
					@SuppressWarnings("rawtypes")
					DefaultComboBoxModel model = (DefaultComboBoxModel) productFromTypeComboboxPanel.getModel();
					Integer index = model.getIndexOf(tableProduct.getValueAt(selectedRow[0], 0));
					productFromTypeComboboxPanel.setSelectedIndex(index);
					System.out.println("Selected: " + tableProduct.getValueAt(selectedRow[0], 0));
				}
			}

		});
		
		tableProduct.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			TABLE_NAMES
		));
		tableProduct.setPreferredScrollableViewportSize(new Dimension(128, 70));
		
		bottomInfoPanel = new JPanel();
		getContentPane().add(bottomInfoPanel, BorderLayout.SOUTH);
		bottomInfoPanel.setLayout(new GridLayout(2, 1, 4, 4));
		
		topStatsStanfordNLPLabel = new JLabel("Stats: ");
		topStatsStanfordNLPLabel.setHorizontalAlignment(SwingConstants.LEFT);
		bottomInfoPanel.add(topStatsStanfordNLPLabel);
		
		topStatsOpenNLPLabel = new JLabel("Stats: ");
		topStatsOpenNLPLabel.setHorizontalAlignment(SwingConstants.LEFT);
		bottomInfoPanel.add(topStatsOpenNLPLabel);
	}
	
	public int getEstimationSimpleSplit() {
		return estimationSimpleSplit;
	}

	private class TypeComboItemListener implements ItemListener {
		// This method is called only if a new item has been selected.
		public void itemStateChanged(ItemEvent evt) {
			Object item = evt.getItem();
			if (evt.getStateChange() == ItemEvent.SELECTED) {
				DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<Object>();
				Collection<ProductItem> prdItemList = xmlDEProducts.getProductListByProducer((String) item);
				Object[][] objForTable = new Object[prdItemList.size()][4];
				int counter = 0;
				for (ProductItem productItem : prdItemList) {
					model.addElement(productItem.getName());
					if(!productItem.getName().equals("<<Empty>>")) {
						objForTable[counter][0] = productItem.getName();
						objForTable[counter][1] = productItem.getPrice();
						objForTable[counter][2] = productItem.getReviewCount();
						objForTable[counter][3] = (String) item;
						counter++;
					}
				}
				tableProduct.setModel(new DefaultTableModel(objForTable, TABLE_NAMES));
				productFromTypeComboboxPanel.setModel(model);
				if(productFromTypeComboboxPanel.getSelectedIndex() != -1)
					infoLabel.setText((String) item + ": " + productFromTypeComboboxPanel.getSelectedItem());
				if(!typeComboboxPanel.getSelectedItem().equals("<<Empty>>")) {
					btnGetReviewByType.setEnabled(true);
				} else {
					btnGetReviewByType.setEnabled(false);
					btnGetReviewByProduct.setEnabled(false);
				}
			} else if (evt.getStateChange() == ItemEvent.DESELECTED) {
				
			}
		}
	}
	
	private class ProductComboItemListener implements ItemListener {
		// This method is called only if a new item has been selected.
		public void itemStateChanged(ItemEvent evt) {
			Object item = evt.getItem();
			if (evt.getStateChange() == ItemEvent.SELECTED) {
				if(productFromTypeComboboxPanel.getSelectedIndex() != -1)
					infoLabel.setText(typeComboboxPanel.getSelectedItem() + ": " + (String) item);
				if(!productFromTypeComboboxPanel.getSelectedItem().equals("<<Empty>>")) {
					btnGetReviewByProduct.setEnabled(true);
					DefaultTableModel model = (DefaultTableModel) tableProduct.getModel();
					for (int i = 0; i < model.getRowCount(); i++) {
						String prdName = (String) model.getValueAt(i, 0);
						if(prdName.equals((String) item)) {
							tableProduct.getSelectionModel().setSelectionInterval(i, i);
							break;
						}
					}
				} else {
					btnGetReviewByProduct.setEnabled(false);
					tableProduct.getSelectionModel().clearSelection();
				}
			} else if (evt.getStateChange() == ItemEvent.DESELECTED) {
				
			}
		}
	}
	
	private class StatsByTypeActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			String type = (String) typeComboboxPanel.getSelectedItem();
			Collection<ProductWithSentimentItem> itemList = null;
			OpenNLPCategorizer reviewCategorizer = new OpenNLPCategorizer();
			reviewCategorizer.trainModel();
			try {
				itemList = xmlDEReviews.getProductReviewByType(type);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
			if(itemList != null) {
				int[] markCount = {0, 0, 0, 0, 0};
				int[] markOpenNLPCount = {0, 0, 0, 0, 0};
				for (ProductWithSentimentItem productItem : itemList) {
					String review = productItem.getProductReview();
					String[] reviewArr = new String[2];
					if(review != null && !review.equals("")) {
						reviewArr[0] = review.substring(0, review.indexOf("|"));
						reviewArr[1] = review.substring(review.indexOf("|") + 1, review.length());
						if(reviewArr != null && reviewArr.length > 1) {
							for (int i = 0; i < reviewArr.length; i++) {
								int sentiment = NLP.findSentiment(reviewArr[i]);
								int openNLPsentiment = reviewCategorizer.classifyNewReview(reviewArr[i]);
								markCount[sentiment]++;
								markOpenNLPCount[openNLPsentiment]++;
								if(i != 0) {
									System.out.println(ConstantValues.INFO_STANFORD_NLP + sentiment + " " + ConstantValues.SEPARATOR_ADD + reviewArr[i]);
									System.out.println(ConstantValues.INFO_OPEN_NLP + openNLPsentiment + " " + ConstantValues.SEPARATOR_ADD + reviewArr[i]);
								} else {
									System.out.println(ConstantValues.INFO_STANFORD_NLP + sentiment + " " + ConstantValues.SEPARATOR + reviewArr[i]);
									System.out.println(ConstantValues.INFO_OPEN_NLP + openNLPsentiment + " " + ConstantValues.SEPARATOR + reviewArr[i]);
								}
							}
						}
					}
				}
				topStatsStanfordNLPLabel.setText(
						ConstantValues.INFO_STATS + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_V_NEGATIVE +
						ConstantValues.INFO_SPACE +  
						markCount[0] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_NEGATIVE + 
						ConstantValues.INFO_SPACE + 
						markCount[1] +
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_NEUTRAL + 
						ConstantValues.INFO_SPACE + 
						markCount[2] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_POSITIVE + 
						ConstantValues.INFO_SPACE + 
						markCount[3] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_V_POSITIVE +
						ConstantValues.INFO_SPACE + 
						markCount[4] +
						ConstantValues.INFO_SPACE 
						);
				topStatsOpenNLPLabel.setText(
						ConstantValues.INFO_STATS + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_V_NEGATIVE +
						ConstantValues.INFO_SPACE +  
						markOpenNLPCount[0] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_NEGATIVE + 
						ConstantValues.INFO_SPACE + 
						markOpenNLPCount[1] +
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_NEUTRAL + 
						ConstantValues.INFO_SPACE + 
						markOpenNLPCount[2] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_POSITIVE + 
						ConstantValues.INFO_SPACE + 
						markOpenNLPCount[3] + 
						ConstantValues.INFO_SPACE + 
						ConstantValues.INFO_V_POSITIVE +
						ConstantValues.INFO_SPACE + 
						markOpenNLPCount[4] +
						ConstantValues.INFO_SPACE 
						);
			}
		}

	}
	
	private class StatsByProductActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			String review = "I hope this lasts as long as my last shower.";
			OpenNLPCategorizer reviewCategorizer = new OpenNLPCategorizer();
			reviewCategorizer.trainModel();
			int sentiment = NLP.findSentiment(review);
			int openNLPsentiment = reviewCategorizer.classifyNewReview(review);
			System.out.println(ConstantValues.INFO_STANFORD_NLP + sentiment + " " + ConstantValues.SEPARATOR + review);
			System.out.println(ConstantValues.INFO_OPEN_NLP + openNLPsentiment + " " + ConstantValues.SEPARATOR + review);
		}
	}

	private class StatsByFileActionListener implements ActionListener {

		Map<Integer, Integer> diffStanfordExperMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> diffSentenceSplitExperMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> diffStanfordSentenceSplitMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> diffSentenceSplitNoCompanyExperMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> diffSentenceSplitNoCompanyMedianExperMap = new HashMap<Integer, Integer>();
		
		public StatsByFileActionListener() {
			super();
			diffStanfordExperMap = new HashMap<Integer, Integer>();
			diffStanfordSentenceSplitMap = new HashMap<Integer, Integer>();
			diffSentenceSplitExperMap = new HashMap<Integer, Integer>();
			diffSentenceSplitNoCompanyExperMap = new HashMap<Integer, Integer>();
			diffSentenceSplitNoCompanyMedianExperMap = new HashMap<Integer, Integer>();
			for (int paramForMap : ConstantValues.STAT_TABLE_OF_PARAM) {
				diffStanfordExperMap.put(paramForMap, 0);
				diffStanfordSentenceSplitMap.put(paramForMap, 0);
				diffSentenceSplitExperMap.put(paramForMap, 0);
				diffSentenceSplitNoCompanyExperMap.put(paramForMap, 0);
				diffSentenceSplitNoCompanyMedianExperMap.put(paramForMap, 0);
			}
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<ProductItem> generalProduct = new ArrayList<ProductItem>();
			List<ProductWithSentimentItem> piwsList = getProductReviewListByName(generalProduct);
			BufferedWriter out = null;
			try {
				out = new BufferedWriter(new FileWriter(ConstantValues.LOCAL_PREFIX_PATH + "output.txt"));
			} catch (IOException err) {
				err.printStackTrace();
			}
			for (ProductWithSentimentItem productWithSentimentItem : piwsList) {
				int parameterSplitSentenceReview = 
						(productWithSentimentItem.getSplitNLPSentiment() != -1) ? 
							productWithSentimentItem.getSplitNLPSentiment() : productWithSentimentItem.getStanfordNLPSentiment();
				int parameterSplitSentenceReviewNoCompany = 
						(productWithSentimentItem.getSplitNLPSentimentNoCompany() != -1) ? 
							productWithSentimentItem.getSplitNLPSentimentNoCompany() : productWithSentimentItem.getStanfordNLPSentiment();
				int parameterSplitSentenceReviewNoCompanyMedian = 
						(productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian() != -1) ? 
							productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian() : productWithSentimentItem.getStanfordNLPSentiment();
				String paramString = productWithSentimentItem.getName() + ";"
						+ productWithSentimentItem.getExpertSentiment() + ";" 
						+ productWithSentimentItem.getStanfordNLPSentiment() + ";" 
						+ parameterSplitSentenceReview + ";" 
						+ parameterSplitSentenceReviewNoCompany + ";"
						+ parameterSplitSentenceReviewNoCompanyMedian + ";"
						+ productWithSentimentItem.getProductReview() 
						+ "\n"; 
				if(out != null) {
					try {
						out.append(paramString);
					} catch (IOException err) {
						err.printStackTrace();
					}
				}
				System.out.print(paramString);
				recountMapsWithDiff(productWithSentimentItem);
			}
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM)
				System.out.print(parameterValue + "\t");
			System.out.println(" ");
			System.out.println("Expert - Stanford: " + "\t");
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM) {
				System.out.print(diffStanfordExperMap.get(parameterValue) + "\t");
			}
			System.out.println(" ");
			System.out.println("Stanford - Split: " + "\t");
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM) {
				System.out.print(diffStanfordSentenceSplitMap.get(parameterValue) + "\t");
			}
			System.out.println(" ");
			System.out.println("Expert - Split: " + "\t");
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM) {
				System.out.print(diffSentenceSplitExperMap.get(parameterValue) + "\t");
			}
			System.out.println(" ");
			System.out.println("Expert - Split No Company: " + "\t");
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM) {
				System.out.print(diffSentenceSplitNoCompanyExperMap.get(parameterValue) + "\t");
			}
			System.out.println(" ");
			System.out.println("Expert - Split No Company Median: " + "\t");
			System.out.print(ConstantValues.SEPARATOR + "\t");
			for (int parameterValue : ConstantValues.STAT_TABLE_OF_PARAM) {
				System.out.print(diffSentenceSplitNoCompanyMedianExperMap.get(parameterValue) + "\t");
			}
			
			if(out != null) {
				try {
					out.close();
				} catch (IOException err) {
					err.printStackTrace();
				}
			}
			findProductComparison(generalProduct, piwsList);
			for (ProductItem generalItem : generalProduct) {
				System.out.print("\n");
				for (int i = 0; i < 50; i++)
					if(i != 25)
						System.out.print(ConstantValues.SIMPLE_SIGN);
					else
						System.out.print("\t\tProduct\t\t");
				System.out.print("\n Product name: " + generalItem.getName() + "\n");
				System.out.print("\n Better products: \n" + generalItem.arrOfProductsToString(generalItem.getReviewListBetter()));
				System.out.print("\n Worst products: \n" + generalItem.arrOfProductsToString(generalItem.getReviewListWorst()));
				System.out.print("\n Other products: \n" + generalItem.arrOfProductsToString(generalItem.getReviewListUndef()));
				for (int i = 0; i < 50; i++)
					if(i != 25)
						System.out.print(ConstantValues.SIMPLE_SIGN);
					else
						System.out.print("\t\tEnd of product\t");
			}
		}

		private void findProductComparison(List<ProductItem> generalProduct, List<ProductWithSentimentItem> piwsList) {
			for (ProductItem generalItem : generalProduct) {
				String[] productNameArr = generalItem.getName().split(" ");
				String[] productTypeArr = generalItem.getProductType().split(" ");
				String namePartsGPrThatEqual = new String();
				String namePartsAPrThatEqual = new String();
				for (ProductWithSentimentItem productWithSentimentItem : piwsList) {
					String actualProductName = productWithSentimentItem.getName();
					String[] actualProductNameArr = actualProductName.split(" ");
					if(!actualProductName.equals(generalItem.getName()) && productWithSentimentItem.getReviewType() == ConstantValues.REVIEW_TYPE_DESCRIPTION) {
						List<String> reviewWithOtherProductList = productWithSentimentItem.getReviewOtherProductArray();
						if(reviewWithOtherProductList != null && !reviewWithOtherProductList.isEmpty()) {
							for (String compSentence : reviewWithOtherProductList) {
								int matchCountForGeneralProduct = 0;
								int matchCountForActualProduct = 0;
								namePartsGPrThatEqual = new String();
								namePartsAPrThatEqual = new String();
								for (String namePart : actualProductNameArr) {
									namePart = namePart.replaceAll("[,.!?()]", "").toLowerCase();
									String lowerTmpReview = compSentence.toLowerCase();
									String[] lowerTmpReviewArr = lowerTmpReview.split(" ");
									for (String lowRevWord : lowerTmpReviewArr) {
										boolean isArrContainsProductType = false;
										String tmwLowRevWord = lowRevWord.replaceAll("[,.!?()]", "").toLowerCase();
										for (String type : productTypeArr) {
											if(type.toLowerCase().contains(tmwLowRevWord)) {
												isArrContainsProductType = true;
												break;
											}
										}
										if(!isArrContainsProductType && lowRevWord.equals(namePart.toLowerCase())) {
											namePartsAPrThatEqual += "Part: " + namePart + "\n";
											matchCountForActualProduct++;
											break;
										}
									}
								}
								for (String namePart : productNameArr) {
									namePart = namePart.replaceAll("[,.!?()]", "").toLowerCase();
									String lowerTmpReview = compSentence.toLowerCase();
									String[] lowerTmpReviewArr = lowerTmpReview.split(" ");
									for (String lowRevWord : lowerTmpReviewArr) {
										boolean isArrContainsProductType = false;
										String tmwLowRevWord = lowRevWord.replaceAll("[,.!?()]", "").toLowerCase();
										for (String type : productTypeArr) {
											if(type.toLowerCase().contains(tmwLowRevWord)) {
												isArrContainsProductType = true;
												break;
											}
										}
										if(!isArrContainsProductType && lowRevWord.equals(namePart.toLowerCase())) {
											namePartsGPrThatEqual += "Part: " + namePart + "\n";
											matchCountForGeneralProduct++;
											break;
										}
									}
								}
								/*String fullReview = productWithSentimentItem.getProductReview();
								String fullReviewLow = fullReview.toLowerCase();
								int oldMatchCountForGeneralProduct = matchCountForGeneralProduct;
								if(matchCountForGeneralProduct < ConstantValues.MATCH_WORD_COUNT && matchCountForGeneralProduct >= 1) {
									String generalProducerLow = generalItem.getProductProducer().toLowerCase();
									String actualProducerLow = productWithSentimentItem.getProductProducer().toLowerCase();
									if(fullReviewLow.contains(generalProducerLow)) {
										if(!generalProducerLow.equals(actualProducerLow))
											matchCountForGeneralProduct = ConstantValues.MATCH_WORD_COUNT;
									}
								}*/
								if(/*matchCountForActualProduct <= matchCountForGeneralProduct &&*/ matchCountForGeneralProduct >= ConstantValues.MATCH_WORD_COUNT) {
									int parameterSplitSentenceReviewNoCompanyMedian = 
										(productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian() != -1) ? 
											productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian() : productWithSentimentItem.getStanfordNLPSentiment();
									if(parameterSplitSentenceReviewNoCompanyMedian < 2) {
										generalItem.addToReviewListBetter(productWithSentimentItem);
									} else if(parameterSplitSentenceReviewNoCompanyMedian > 2) {
										generalItem.addToReviewListWorst(productWithSentimentItem);
									} else {
										generalItem.addToReviewListUndef(productWithSentimentItem);
									}
									System.out.print("\n");
									for (int i = 0; i < 50; i++)
										if(i != 25)
											System.out.print(ConstantValues.SIMPLE_SIGN);
										else
											System.out.print("\t\tProduct\t\t");
									System.out.print("\n" + generalItem.getName() + "\n\t compared with \n" + productWithSentimentItem.getName());
									System.out.print("\nWith match count: " + matchCountForGeneralProduct + /*" old match count (" + oldMatchCountForGeneralProduct + ")*/"\n");
									System.out.print("Name parts for general product: \n");
									System.out.print((namePartsGPrThatEqual.equals("") ? ConstantValues.INFO_EMPTY : namePartsGPrThatEqual) + "\n");
									System.out.print("Name parts for actual product: \n");
									System.out.print((namePartsAPrThatEqual.equals("") ? ConstantValues.INFO_EMPTY : namePartsAPrThatEqual) + "\n");
									System.out.print("Comparison sentence: " + compSentence + "\n");
									System.out.print("Full review: " + productWithSentimentItem.getProductReview() + "\n");
									for (int i = 0; i < 50; i++)
										if(i != 25)
											System.out.print(ConstantValues.SIMPLE_SIGN);
										else
											System.out.print("\t\tEnd of product\t");
								}
							}
						} else {
							continue;
						}
					}
				}
			}
		}

		private void recountMapsWithDiff(ProductWithSentimentItem productWithSentimentItem) {
			// Expert - Value = diff
			if(productWithSentimentItem.getStanfordNLPSentiment() < 0)
				productWithSentimentItem.setStanfordNLPSentiment(2);
			if(productWithSentimentItem.getExpertSentiment() < 0)
				productWithSentimentItem.setExpertSentiment(2);
			if(productWithSentimentItem.getSplitNLPSentiment() < 0)
				productWithSentimentItem.setSplitNLPSentiment(2);
			if(productWithSentimentItem.getSplitNLPSentimentNoCompany() < 0)
				productWithSentimentItem.setSplitNLPSentimentNoCompany(2);
			if(productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian() < 0)
				productWithSentimentItem.setSplitNLPSentimentNoCompanyMedian(2);
			int stanfordExperDiff = productWithSentimentItem.getExpertSentiment() - productWithSentimentItem.getStanfordNLPSentiment();
			int stanfordSentenceSplitDiff = productWithSentimentItem.getStanfordNLPSentiment() - productWithSentimentItem.getSplitNLPSentiment();
			int sentenceSplitExperDiff = productWithSentimentItem.getExpertSentiment() - productWithSentimentItem.getSplitNLPSentiment();
			int sentenceSplitNoCompanyExperDiff = productWithSentimentItem.getExpertSentiment() - productWithSentimentItem.getSplitNLPSentimentNoCompany();
			int sentenceSplitNoCompanyMedianExperDiff = productWithSentimentItem.getExpertSentiment() - productWithSentimentItem.getSplitNLPSentimentNoCompanyMedian();
			int currentValue = diffStanfordExperMap.get(stanfordExperDiff) + 1;
			diffStanfordExperMap.put(stanfordExperDiff, currentValue);
			currentValue = diffStanfordSentenceSplitMap.get(stanfordSentenceSplitDiff) + 1;
			diffStanfordSentenceSplitMap.put(stanfordSentenceSplitDiff, currentValue);
			currentValue = diffSentenceSplitExperMap.get(sentenceSplitExperDiff) + 1;
			diffSentenceSplitExperMap.put(sentenceSplitExperDiff, currentValue);
			currentValue = diffSentenceSplitNoCompanyExperMap.get(sentenceSplitNoCompanyExperDiff) + 1;
			diffSentenceSplitNoCompanyExperMap.put(sentenceSplitNoCompanyExperDiff, currentValue);
			currentValue = diffSentenceSplitNoCompanyMedianExperMap.get(sentenceSplitNoCompanyMedianExperDiff) + 1;
			diffSentenceSplitNoCompanyMedianExperMap.put(sentenceSplitNoCompanyMedianExperDiff, currentValue);
		}

		/* --------------------------------------- This methods help with object transformations  ---------------------------------------------- */
		
		public boolean isNumeric(String str) {
			try {
				Integer.parseInt(str);
			} catch (NumberFormatException nfe) {
				return false;
			}
			return true;
		}
		
		private int regexpIndex(String line, String regexp) {
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(line);
			int indexOfExp = 0;
			if(matcher.find())
				indexOfExp = matcher.start();
			return indexOfExp;
		}

		public int median(List<Integer> listOfMarks) {
			Object[] array = listOfMarks.toArray();
			int middle = array.length / 2;
			if (array.length % 2 == 0) {
				int left = (int) array[middle - 1];
				int right = (int) array[middle];
				return (left + right) / 2;
			} else {
				return (int) array[middle];
			}
		}
		
		/* --------------------------------------- This methods help with object transformations  ---------------------------------------------- */
		
		private List<ProductWithSentimentItem> getProductReviewListByName(List<ProductItem> generalProduct) {
			ArrayList<ProductWithSentimentItem> piwsList = new ArrayList<ProductWithSentimentItem>();
			File txtSource = new File(/*ConstantValues.DISC_PREFIX_PATH + ConstantValues.FTP_DATA_PATH*/ ConstantValues.LOCAL_PREFIX_PATH + ConstantValues.TXT_SOURCE_FILE);
			Path file = txtSource.toPath();
			try (InputStream in = Files.newInputStream(file);
				BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
				String line = null;
				int iterationCount = 0;
				ProductItem generalItem = new ProductItem();
				while ((line = reader.readLine()) != null) {
					iterationCount++;
					generalItem = new ProductItem();
					if(!line.equals("")) {
						//System.out.println(line);		
						String productInfo = line.substring(0, regexpIndex(line, ConstantValues.REGEXP_MARK_WITH_ARROW));
						String productName = productInfo.split(" Price: ")[0].split("Product name: ")[1];
						String productType = productInfo.split(" ProductType: ")[1];
						String reviewString = line.substring(regexpIndex(line, ConstantValues.REGEXP_MARK_WITH_ARROW), line.length());
						ProductWithSentimentItem piws = new ProductWithSentimentItem();
						piws.setName(productName);
						piws.setProductType(productType);
						generalItem.setName(productName);
						generalItem.setProductType(productType);
						for (String producer : xmlDEProducers.getProductProducers()) {
							if(productName.toLowerCase().contains(producer.toLowerCase())) {
								piws.setProductProducer(producer);
								generalItem.setProductProducer(producer);
								break;
							} else {
								piws.setProductProducer("");
								generalItem.setProductProducer("");
							}
						}
						String workStringMark = reviewString.substring(0, regexpIndex(reviewString, ConstantValues.REGEXP_ARROW));
						workStringMark = workStringMark.trim();
						reviewString = reviewString.substring(regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW) + 6, reviewString.length());
						reviewString = reviewString.trim();
						String workString = reviewString.substring(0, regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW));
						reviewString = reviewString.substring(regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW), reviewString.length());
						reviewString = reviewString.trim();
						int reviewIterator = 1;
						while(!workString.equals("")) {
							
							workString.trim();
							piws = null;
							piws = new ProductWithSentimentItem();
							piws.setName(productName);
							for (String producer : xmlDEProducers.getProductProducers()) {
								if(productName.toLowerCase().contains(producer.toLowerCase())) {
									piws.setProductProducer(producer);
									break;
								} else {
									piws.setProductProducer("");
								}
							}
							if(isNumeric(workStringMark)) {
								piws.setExpertSentiment(Integer.parseInt(workStringMark));
							}
							
							//System.out.println(productInfo);
							//System.out.println(reviewString);
							if(workString.contains("ReviewTitle: ")) {
								String[] reviewTitle = workString.split("ReviewTitle: ");
								piws.setProductReview(reviewTitle[1]);
								piws.setReviewType(ConstantValues.REVIEW_TYPE_TITLE);
								piws.setStanfordNLPSentiment(NLP.findSentiment(reviewTitle[1]));
								
								// Count product review mark with sentence split
								// int summary = parseDescriptionAndGetSummaryMark(reviewTitle[1], ConstantValues.ALGORITHM_SPLIT);
								parseDescriptionAndGetSummaryMark(reviewTitle[1]);
								piws.setReviewOtherProductArray(getReviewOtherProductArray());
								piws.setSplitNLPSentiment(getEstimationSimpleSplit());
								// Count product review mark with sentence split and removing sentence with company name
								// summary = parseDescriptionAndGetSummaryMark(reviewTitle[1], ConstantValues.ALGORITHM_SPLIT_REMOVE_COMPANY);
								piws.setSplitNLPSentimentNoCompany(getEstimationSimpleSplitWithoutCompany());
								// Count product review mark with sentence split and removing sentence with company name chose median value
								// summary = parseDescriptionAndGetSummaryMark(reviewTitle[1], ConstantValues.ALGORITHM_SPLIT_MEDIAN);
								piws.setSplitNLPSentimentNoCompanyMedian(getEstimationSimpleSplitWithoutCompanyMedian());
								
								piws.setReviewConnector(reviewIterator);
								int parameterSplitSentenceReview = 
										(piws.getSplitNLPSentiment() != -1) ? 
												piws.getSplitNLPSentiment() : piws.getStanfordNLPSentiment();
								int parameterSplitSentenceReviewNoCompany = 
										(piws.getSplitNLPSentimentNoCompany() != -1) ? 
												piws.getSplitNLPSentimentNoCompany() : piws.getStanfordNLPSentiment();
								int parameterSplitSentenceReviewNoCompanyMedian = 
										(piws.getSplitNLPSentimentNoCompanyMedian() != -1) ? 
												piws.getSplitNLPSentimentNoCompanyMedian() : piws.getStanfordNLPSentiment();
								String markString = 
									"\nExpert => " + piws.getExpertSentiment()
									+ "\nStanfordNLP => " + piws.getStanfordNLPSentiment()
									+ "\nSplit => " + parameterSplitSentenceReview
									+ "\nSplitNoCompany => " + parameterSplitSentenceReviewNoCompany
									+ "\nSplitNoCompanyMedian => " + parameterSplitSentenceReviewNoCompanyMedian;
								generalItem.addToReviewList(new String[]{ markString, reviewTitle[1]});
							} else if(workString.contains("ReviewSummary: ")) {
								String[] reviewDesc = workString.split("ReviewSummary: ");
								piws.setProductReview(reviewDesc[1]);
								piws.setReviewType(ConstantValues.REVIEW_TYPE_DESCRIPTION);
								piws.setReviewConnector(reviewIterator);
								piws.setStanfordNLPSentiment(NLP.findSentiment(reviewDesc[1]));
								
								// Count product review mark with sentence split
								// int summary = parseDescriptionAndGetSummaryMark(reviewDesc[1], ConstantValues.ALGORITHM_SPLIT);
								parseDescriptionAndGetSummaryMark(reviewDesc[1]);
								piws.setReviewOtherProductArray(getReviewOtherProductArray());
								piws.setSplitNLPSentiment(getEstimationSimpleSplit());
								// Count product review mark with sentence split and removing sentence with company name
								// summary = parseDescriptionAndGetSummaryMark(reviewDesc[1], ConstantValues.ALGORITHM_SPLIT_REMOVE_COMPANY);
								piws.setSplitNLPSentimentNoCompany(getEstimationSimpleSplitWithoutCompany());
								// Count product review mark with sentence split and removing sentence with company name chose median value
								// summary = parseDescriptionAndGetSummaryMark(reviewDesc[1], ConstantValues.ALGORITHM_SPLIT_MEDIAN);
								piws.setSplitNLPSentimentNoCompanyMedian(getEstimationSimpleSplitWithoutCompanyMedian());
								int parameterSplitSentenceReview = 
										(piws.getSplitNLPSentiment() != -1) ? 
												piws.getSplitNLPSentiment() : piws.getStanfordNLPSentiment();
								int parameterSplitSentenceReviewNoCompany = 
										(piws.getSplitNLPSentimentNoCompany() != -1) ? 
												piws.getSplitNLPSentimentNoCompany() : piws.getStanfordNLPSentiment();
								int parameterSplitSentenceReviewNoCompanyMedian = 
										(piws.getSplitNLPSentimentNoCompanyMedian() != -1) ? 
												piws.getSplitNLPSentimentNoCompanyMedian() : piws.getStanfordNLPSentiment();
								String markString = 
									"\nExpert => " + piws.getExpertSentiment()
									+ "\nStanfordNLP => " + piws.getStanfordNLPSentiment()
									+ "\nSplit => " + parameterSplitSentenceReview
									+ "\nSplitNoCompany => " + parameterSplitSentenceReviewNoCompany
									+ "\nSplitNoCompanyMedian => " + parameterSplitSentenceReviewNoCompanyMedian;
								generalItem.addToReviewList(new String[]{ markString, reviewDesc[1]});
								reviewIterator += 2;
							} 
							piwsList.add(piws);
							if(reviewString != null && !reviewString.equals("")) {
								workStringMark = reviewString.substring(0, regexpIndex(reviewString, ConstantValues.REGEXP_ARROW));
								workStringMark = workStringMark.trim();
								reviewString = reviewString.substring(regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW) + 6, reviewString.length());
								reviewString = reviewString.trim();
								workString = reviewString.substring(0, regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW));
								if((workString == null || workString.equals("")) && (reviewString != null || !reviewString.equals(""))) {
									workString = reviewString;
									reviewString = ""; 
								}
								reviewString = reviewString.substring(regexpIndex(reviewString, ConstantValues.REGEXP_MARK_WITH_ARROW), reviewString.length());
								reviewString = reviewString.trim();
							} else {
								workString = "";
							}
						}
						generalItem.setReviewCount(generalItem.getReviewList().size());
						generalProduct.add(generalItem);
					}
					System.out.println("Product scan... " + "# " + iterationCount);
				}
			} catch (IOException x) {
				System.err.println(x);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
			return piwsList;
		}

		private void parseDescriptionAndGetSummaryMark(String review) {
			setReviewOtherProductArray(new ArrayList<String>());
			// Ustawianie warto�ci ocen dla poszczeg�lnych metod na -1
			setEstimationSimpleSplit(-1);
			setEstimationSimpleSplitWithoutCompany(-1);
			setEstimationSimpleSplitWithoutCompanyMedian(-1);
			// Definicja i inicjalizacja oceny
			int mark = 0;
			// Lista zda� opinii
			List<String> dp = NLP.getSentenceListFromString(review);
			// Lista ocen dla metody "Split"
			ArrayList<Integer> listOfMarksForSplit = new ArrayList<Integer>();
			// Lista ocen dla metody "Split No Company" i "Split No Company Median"
			ArrayList<Integer> listOfMarksForSplitNoCompany = new ArrayList<Integer>();
			// Analiza ka�dego zdania
			for (String sentence : dp) {
				// Zmienna kt�ra wskazuje czy sentyment ma by� wyznaczony dla zdania
				boolean findStanfordSentimentForSentence = true;
				// Zdanie ma�ymi literami
				String lowerSentence = sentence.toLowerCase();
				try {
					// Kod wykorzystywany przy analizie metod� "Split No Company Median"
					for (String producer : xmlDEProducers.getProductProducers()) 
						if(lowerSentence.contains(producer.toLowerCase())) {
							getReviewOtherProductArray().add(sentence);
							findStanfordSentimentForSentence = false;
							//break;
						}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				} finally {
					// Gdy zmienna ma warto�� 'true' sentyment jest wyznaczany
					if(findStanfordSentimentForSentence)
						listOfMarksForSplitNoCompany.add(NLP.findSentiment(sentence));
					// Dodawanie warto�ci sentymenty do listy ocen metody "Split"
					listOfMarksForSplit.add(NLP.findSentiment(sentence));
				}
			}
			
			// Wyznaczanie sentymetu ko�cowego metod� "Split"
			if(!listOfMarksForSplit.isEmpty()) {
				for (Integer i : listOfMarksForSplit)
					mark += i;
				setEstimationSimpleSplit(mark/listOfMarksForSplit.size());
			}
			
			// Przypisanie zerowej warto�ci do zmiennej oceny
			mark = 0;
			
			// Wyznaczanie sentymetu ko�cowego metodami "Split No Company" i "Split No Company Median"
			if(!listOfMarksForSplitNoCompany.isEmpty()) {
				for (Integer i : listOfMarksForSplitNoCompany)
					mark += i;
				setEstimationSimpleSplitWithoutCompany(mark/listOfMarksForSplitNoCompany.size());
				Collections.sort(listOfMarksForSplitNoCompany);
				setEstimationSimpleSplitWithoutCompanyMedian(median(listOfMarksForSplitNoCompany));
			}
		}
		
	}
	
	public void setEstimationSimpleSplit(int estimationSimpleSplit) {
		this.estimationSimpleSplit = estimationSimpleSplit;
	}

	public int getEstimationSimpleSplitWithoutCompany() {
		return estimationSimpleSplitWithoutCompany;
	}

	public void setEstimationSimpleSplitWithoutCompany(int estimationSimpleSplitWithoutCompany) {
		this.estimationSimpleSplitWithoutCompany = estimationSimpleSplitWithoutCompany;
	}

	public int getEstimationSimpleSplitWithoutCompanyMedian() {
		return estimationSimpleSplitWithoutCompanyMedian;
	}

	public void setEstimationSimpleSplitWithoutCompanyMedian(int estimationSimpleSplitWithoutCompanyMedian) {
		this.estimationSimpleSplitWithoutCompanyMedian = estimationSimpleSplitWithoutCompanyMedian;
	}
	
	public List<String> getReviewOtherProductArray() {
		return reviewOtherProductArray;
	}
	
	public void setReviewOtherProductArray(List<String> reviewOtherProductArray) {
		this.reviewOtherProductArray = reviewOtherProductArray;
	}
	
	public void addToReviewOtherProductArray(String reviewOtherProduct) {
		reviewOtherProductArray.add(reviewOtherProduct);
	}
	
	public void removeFromReviewOtherProductArray(String reviewOtherProduct) {
		this.reviewOtherProductArray.remove(reviewOtherProduct);
	}
}
